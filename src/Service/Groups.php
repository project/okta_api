<?php

declare(strict_types = 1);

namespace Drupal\okta_api\Service;

use Okta\Exception as OktaException;
use Okta\Resource\Group;

/**
 * Class Groups.
 *
 * @package Drupal\okta_api\Service
 */
class Groups {

  /**
   * The Okta client API.
   *
   * @var \Okta\Client
   */
  public $oktaClient;

  /**
   * The Okta group API resource.
   *
   * @var \Okta\Resource\Group
   */
  public $oktaGroup;

  /**
   * Constructor for the Okta Users class.
   *
   * @param \Drupal\okta_api\Service\OktaClient $oktaClient
   *   An OktaClient.
   */
  public function __construct(OktaClient $oktaClient) {
    $this->oktaClient = $oktaClient->Client;
    $this->oktaGroup = new Group($oktaClient->Client);
  }

  /**
   * Retrieve a list of groups from Okta.
   *
   * @param array $search
   *   Associative array of search parameters. Use the 'q' key to filter a group
   *   by name. Use the 'limit' key to limit the number of results returned.
   *   Pass an empty array to get all groups within Okta.
   *
   * @return array
   *   An array of the group object.
   */
  public function listGroups(array $search): array {
    try {
      $groups = $this->oktaGroup->listGroups($search);
    }
    catch (OktaException $e) {
      $groups = [];
    }

    return $groups;
  }

  /**
   * Add a user to a group.
   *
   * @param string $gid
   *   The group id as defined within Okta.
   * @param string $uid
   *   The user id as defined within Okta.
   *
   * @return bool
   *   Return true if the operation was successful.
   */
  public function addUser(string $gid, string $uid): bool {
    try {
      $group = $this->oktaGroup->addUser($gid, $uid);
    }
    catch (OktaException $e) {
      $group = FALSE;
    }

    // If the group is an empty object, saving was successful.
    if (!is_bool($group)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Remove a user from a group.
   *
   * @param string $gid
   *   The group id as defined within Okta.
   * @param string $uid
   *   The user id as defined within Okta.
   *
   * @return bool
   *   Return true if the operation was successful.
   */
  public function removeUser(string $gid, string $uid): bool {
    try {
      $group = $this->oktaGroup->removeUser($gid, $uid);
    }
    catch (OktaException $e) {
      $group = FALSE;
    }

    // If the group is an empty object, removing was successful.
    if (!is_bool($group)) {
      return TRUE;
    }

    return FALSE;
  }

}
