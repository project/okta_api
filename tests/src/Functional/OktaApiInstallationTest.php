<?php

declare(strict_types=1);

namespace Drupal\Tests\okta_api\Functional;

use Drupal\Tests\BrowserTestBase;

class OktaApiInstallationTest extends BrowserTestBase {

    /**
    * {@inheritdoc}
    */
    protected static $modules = [
      'okta_api',
    ];

    /**
    * {@inheritdoc}
    */
    protected $defaultTheme = 'stark';

  /**
   * The administrative user.
   *
   * @var \Drupal\user\UserInterface
   */
    protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);
  }

  /**
  * Test that the module installs correctly.
  */
  public function testInstallation(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/okta_api/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
  }
}